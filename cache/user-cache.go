package cache

import (
	u "gitlab.com/laoss/go-testask/classes/user"
)

// UserCache ...
type UserCache interface {
	Set(key string, value *u.User)
	Get(key string, field uint64) *u.User
}
