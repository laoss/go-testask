package cache

import (
	"encoding/json"
	"time"

	"github.com/go-redis/redis/v7"
	u "gitlab.com/laoss/go-testask/classes/user"
)

type redisCache struct {
	host    string
	db      int
	expires time.Duration
}

// NewRedisCache ...
func NewRedisCache(host string, db int, exp time.Duration) UserCache {
	return &redisCache{
		host:    host,
		db:      db,
		expires: exp,
	}
}

// getClient ...
func (cache *redisCache) getClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     cache.host,
		Password: "",
		DB:       cache.db,
	})
}

func (cache *redisCache) Set(key string, value *u.User) {
	client := cache.getClient()

	// Здесь добавление юзера просто как объект в json
	json, err := json.Marshal(value)
	if err != nil {
		panic(err)
	}
	client.Set(key, json, cache.expires*time.Second)

	// ПОПЫТКИ ДОБАВИТЬ ЮЗЕРА ЧЕРЕЗ MAP
	//!!! - HMSet("myhash", map[string]interface{}{"key1": "value1", "key2": "value2"})
	// Добавить map с указателями не получается, выбрасывает ошибку что не может маршалить map
	//users := make(map[string]*u.User)
	//
	// users := make(map[string]string)
	// str := fmt.Sprintf("%p", value)
	// users[strconv.Itoa(value.ID)] = str
	// получилось засунуть в хэштабл( как строку с id и строку с укаателем )
	// b := client.HMSet(key, strconv.Itoa(value.ID), users[strconv.Itoa(value.ID)]) // redis: can't marshal map[string]string (implement encoding.BinaryMarshaler)
	// if b.Err() != nil || b.Val() == false {
	// 	fmt.Println("SET isn't done")
	// 	fmt.Println(b.Err())
	// }
	// fmt.Println("SET done")
}

func (cache *redisCache) Get(key string, field uint64) *u.User {
	client := cache.getClient()

	//Получение юзера как JSON
	val, err := client.Get(key).Result()
	if err != nil {
		return nil
	}
	user := u.User{}
	err = json.Unmarshal([]byte(val), &user)
	if err != nil {
		panic(err)
	}
	return &user

	//ПОПЫТКИ РАБОТАТЬ С MAP в редис
	// val, err := client.HGet(key, strconv.FormatUint(field, 10)).Result()
	// if err != nil {
	// 	return nil
	// }
	//
	//var usr *u.User = (*u.User)(val)
	//usr := (*u.User)(val)
	//fmt.Println(val)
	//return nil
}
