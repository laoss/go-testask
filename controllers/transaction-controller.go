package controllers

import (
	"database/sql"
	"errors"
	"strconv"

	_ "github.com/go-sql-driver/mysql" // justifying comment
)

// MakeTransaction (Transaction func in task)
func MakeTransaction(userID int, transactionID int, typeTr string, amount float64, token string) (float64, error) {
	db, err := sql.Open("mysql", "root:@/db_tt")
	if err != nil {
		err = errors.New("Connection error with DB")
		return 0, err
	}
	defer db.Close()

	// Проверки и зaполнение
	var balanceAfter, balanceBefore float64
	if typeTr == "Win" {
		// проверка существует ли такая транз
		var cnt int
		err = db.QueryRow("select count(id) from db_tt.Transactions where id = ?", transactionID).Scan(&cnt)
		if err != nil {
			err = errors.New("Query SELECT(cnt) error")
			return 0, err
		}
		if cnt == 0 {
			err = errors.New("Transaction does not exist")
			return 0, err
		}

		// Достается старый баланс и формируется новый
		err = db.QueryRow("select balance from db_tt.Users where id = ?", userID).Scan(&balanceBefore)
		if err != nil {
			err = errors.New("Query SELECT(User) error")
			return 0, err
		}
		balanceAfter = balanceBefore + amount

		// Обновление тран-ых данных
		_, err = db.Exec("update db_tt.Transactions"+
			" set amount = ?,"+
			" balanceBefore = ?,"+
			" balanceAfter = ?,"+
			" type = ?"+
			" where id = ?",
			amount,
			balanceBefore,
			balanceAfter,
			typeTr,
			transactionID)
		if err != nil {
			err = errors.New("Query UPDATE(Transactions) error")
			return 0, err
		}

		/*
			// Обновление данных пользователя (БД)
			_, err = db.Exec("update db_tt.Users set"+
				" winCount = winCount + 1,"+
				" winSum = winSum + ?,"+
				" balance = ?"+
				" where id = ?",
				amount,
				balanceAfter,
				userID,
			)
		*/

		// Обновление данных пользователя (Cache)
		// получение старых данных из кеша
		usr, err := GetUser(userID, "testtask")
		if err != nil {
			return 0, err
		}
		// обновление значений
		usr.Balance = balanceAfter
		usr.WinCount = usr.WinCount + 1
		usr.WinSum = usr.WinSum + amount
		// сохранение обновленных данных
		UserCache.Set(strconv.Itoa(userID), &usr)

	} else {
		// Проверка уникальности
		var cnt int
		err = db.QueryRow("select count(id) from db_tt.Transactions where id = ?", transactionID).Scan(&cnt)
		if err != nil {
			err = errors.New("Query SELECT(cnt) error")
			return 0, err
		}
		if cnt != 0 {
			err = errors.New("Transaction Id is not free")
			return 0, err
		}

		// Проверка можно ли игроку совершить ставку
		err = db.QueryRow("select balance from db_tt.Users where id = ?", userID).Scan(&balanceBefore)
		if err != nil {
			err = errors.New("Query SELECT(Users) error")
			return 0, err
		}
		if balanceBefore < 0 {
			err = errors.New("Player cannot bid")
			return 0, err
		}

		// Сохранение в табл Ставок(Bets)
		res, err := db.Exec("insert into db_tt.Bets(userId, amount) value(?, ?)", userID, amount)
		if err != nil {
			err = errors.New("Query INSERT(Bets) error")
			return 0, err
		}
		betID, err := res.LastInsertId()
		if err != nil {
			err = errors.New("betId is nil")
			return 0, err
		}

		// Формирование данных и сохранение в базу
		balanceAfter = balanceBefore - amount
		_, err = db.Exec("insert into db_tt.Transactions(id, betId, amount, balanceBefore, balanceAfter, type)"+
			" values(?, ?, ?, ?, ?, ?)",
			transactionID,
			betID,
			amount,
			balanceBefore,
			balanceAfter,
			typeTr)
		if err != nil {
			err = errors.New("Query INSERT(Transactions) error")
			return 0, err
		}

		/*
			// Обновление данных юзера (БД)
			_, err = db.Exec("update db_tt.Users set"+
				" balance = ?,"+
				" betCount = betCount + 1,"+
				" betSum = betSum + ?"+
				" where id = ?",
				balanceAfter,
				amount,
				userID)
			if err != nil {
				err = errors.New("Query UPDATE(Users) errors")
				return 0, err
			}*/

		// Обновление данных пользователя (Cache)
		// получение старых данных из кеша
		usr, err := GetUser(userID, "testtask")
		if err != nil {
			return 0, err
		}
		// обновление значений
		usr.Balance = balanceAfter
		usr.BetCount = usr.BetCount + 1
		usr.BetSum = usr.BetSum + amount
		// сохранение обновленных данных
		UserCache.Set(strconv.Itoa(userID), &usr)
	}

	return balanceAfter, nil
}
