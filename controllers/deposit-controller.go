package controllers

import (
	"database/sql"
	"errors"
	"strconv"

	_ "github.com/go-sql-driver/mysql" // justifying comment
)

// AddDeposit ...
func AddDeposit(userID int, depositID int, amount float64, token string) (float64, error) {
	db, err := sql.Open("mysql", "root:@/db_tt")
	if err != nil {
		err = errors.New("Connection error with DB")
		return 0, err
	}
	defer db.Close()

	// Проверка уникальности
	var cnt int
	err = db.QueryRow("select count(id) from db_tt.Deposits where id = ?", depositID).Scan(&cnt)
	if err != nil {
		err = errors.New("Query SELECT(cnt) error")
		return 0, err
	}
	if cnt != 0 {
		err = errors.New("Deposit with this Id already exists")
		return 0, err
	}

	// Формирование значений
	var balanceBefore float64
	err = db.QueryRow("select balance from db_tt.Users where id = ?", userID).Scan(&balanceBefore)
	if err != nil {
		err = errors.New("User with this ID not exists or query error")
		return 0, err
	}
	balanceAfter := balanceBefore + amount

	// Добавление в базу
	_, err = db.Exec("insert into db_tt.Deposits(id, userId, balanceBefore, balanceAfter)"+
		" values(?, ?, ?, ?)",
		depositID,
		userID,
		balanceBefore,
		balanceAfter,
	)
	if err != nil {
		err = errors.New("Query INSERT error")
		return 0, err
	}

	/*
		// Обновление Users (DB)
		_, err = db.Exec("update db_tt.Users set balance = ?,"+
			" depositCount = depositCount + 1,"+
			" depositSum = depositSum + ?"+
			" where id = ?",
			balanceAfter,
			amount,
			userID,
		)
		if err != nil {
			err = errors.New("Query UPDATE error")
			return 0, err
		}
	*/

	// Обновление Users (Redis)
	// с начала нужно получить данные из кеша
	usr, err := GetUser(userID, "testtask")
	if err != nil {
		return 0, err
	}
	// обновление значений
	usr.Balance = balanceAfter
	usr.DepositCount = usr.DepositCount + 1
	usr.DepositSum = usr.DepositSum + amount
	// сохранение обновленных данных
	UserCache.Set(strconv.Itoa(userID), &usr)

	return balanceAfter, nil
}
