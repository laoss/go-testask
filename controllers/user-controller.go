package controllers

import (
	"database/sql"
	"errors"
	"strconv"

	_ "github.com/go-sql-driver/mysql" // justifying comment
	"gitlab.com/laoss/go-testask/cache"
	u "gitlab.com/laoss/go-testask/classes/user"
)

//type controllers struct{}

var (
	// UserCache ...
	UserCache cache.UserCache = cache.NewRedisCache("localhost:6379", 0, 0)
)

// Key for redis hashMap
//const hashKey string = "usersMap"

// AddUser ...
func AddUser(id int, balance float64, token string) error {
	// Подключение к БД
	db, err := sql.Open("mysql", "root:@/db_tt")
	if err != nil {
		err = errors.New("Connection error with DB")
		return err
	}
	defer db.Close()

	//Проверка уникальности id
	var cnt int
	err = db.QueryRow("select count(id) from db_tt.Users where id = ?", id).Scan(&cnt)
	if err != nil {
		err = errors.New("Query SELECT(cnt) error")
		return err
	}
	if cnt != 0 {
		err = errors.New("User Id is not free")
		return err
	}

	// Добавление в БД
	_, err = db.Exec("insert into db_tt.Users(id, balance) values(?, ?)", id, balance)
	if err != nil {
		err = errors.New("Query INSERT error")
		return err
	}
	// Добавление в кэш
	user := u.User{
		ID:      id,
		Balance: balance,
	}
	UserCache.Set(strconv.Itoa(user.ID), &user)

	return nil
}

//GetUser ...
func GetUser(id int, token string) (u.User, error) {
	var user *u.User = UserCache.Get(strconv.Itoa(id), (uint64)(id))
	if user == nil {
		err := errors.New("User with this id not found")
		return u.User{}, err
	}

	return *user, nil
}

// UpdateUserTbl ...
func UpdateUserTbl(id int, token string) error {
	// Получение юзера из кэша
	user, err := GetUser(id, token)
	if err != nil {
		return err
	}

	// Подключение к БД
	db, err := sql.Open("mysql", "root:@/db_tt")
	if err != nil {
		err = errors.New("Connection error with DB")
		return err
	}
	defer db.Close()

	// Обновление БД
	_, err = db.Exec(`update db_tt.Users set
		balance = ?,
		depositCount = ?,
		depositSum = ?,
		betCount = ?,
		betSum = ?, 
		winCount = ?,
		winSum = ?
		where id = ?`,
		user.Balance,
		user.DepositCount,
		user.DepositSum,
		user.BetCount,
		user.BetSum,
		user.WinCount,
		user.WinSum,
		id,
	)
	if err != nil {
		err = errors.New("Query UPDATE error")
		return err
	}

	return nil
}
