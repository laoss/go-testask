package controllers

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/websocket"
	"gitlab.com/laoss/go-testask/utils"
	"gitlab.com/laoss/go-testask/utils/websockets"
)

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	jwtSecretKey = []byte("topsecret")
)

//HomePage ...
func HomePage(w http.ResponseWriter, r *http.Request) {
	//http.ServeFile(w, r, "view/index.html")
	tmpl, err := template.ParseFiles("index.html")
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	if err := tmpl.Execute(w, nil); err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
}

// CreateUserHandler ...
func CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	//var userCache cache.UserCache = cache.NewRedisCache("localhost:6379", 0, 0)
	//UserCache = userCache

	userID := r.FormValue("userId")
	balance := r.FormValue("balance")

	uID, err := strconv.Atoi(userID)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	uBalance, err := strconv.ParseFloat(balance, 64)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	err = AddUser(uID, uBalance, "testtask")
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	fmt.Fprint(w, "User added successfully")
}

// CreateUserPage ...
func CreateUserPage(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "view/user-create.html")
}

// GetUserHandler ...
func GetUserHandler(w http.ResponseWriter, r *http.Request) {

	userID := r.FormValue("userId")

	uID, err := strconv.Atoi(userID)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	user, err := GetUser(uID, "testtask")
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	newJWToken, err := utils.GenerateJWT(user.ID)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	// использую сессионную куку
	authTokenCookie := &http.Cookie{
		Name:  "authToken",
		Value: newJWToken,
	}
	http.SetCookie(w, authTokenCookie)

	tmpl, err := template.ParseFiles("view/user-geted.html")
	tmpl.Execute(w, user)

}

// GetUserPage ...
func GetUserPage(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "view/user-get.html")
}

// MakeDepositPage ...
func MakeDepositPage(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "view/deposit-add.html")
}

// MakeDepositHandler ...
func MakeDepositHandler(w http.ResponseWriter, r *http.Request) {

	userID := r.FormValue("userId")
	depositID := r.FormValue("depositId")
	amount := r.FormValue("amount")

	uID, err := strconv.Atoi(userID)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	dID, err := strconv.Atoi(depositID)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	amnt, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	balance, err := AddDeposit(uID, dID, amnt, "testtask")
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	fmt.Fprint(w, balance)

	// tmpl, _ := template.New("data").Parse(`
	// 	<p>Deposit was successfull! Balance user - {{ . }}</p>
	// 	<a href="/">Home</a>
	// 	<script>localStorage['userUpdateble'] = true</script>
	// `)
	// tmpl.Execute(w, balance)
}

// MakeTransactionPage ...
func MakeTransactionPage(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "view/transaction.html")
}

// MakeTransactionHandler ...
func MakeTransactionHandler(w http.ResponseWriter, r *http.Request) {

	userID := r.FormValue("userId")
	transactionID := r.FormValue("transactionId")
	amount := r.FormValue("amount")
	typeTr := r.FormValue("type")

	uID, err := strconv.Atoi(userID)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	trID, err := strconv.Atoi(transactionID)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	amnt, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	balance, err := MakeTransaction(uID, trID, typeTr, amnt, "testtask")
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	str := fmt.Sprintf("Type - %s, balance - %f", typeTr, balance)
	tmpl, _ := template.New("data").Parse(`
		<p>Transaction was successfull. {{ . }} </p>
		<a href="/">Home</a>
		<script>localStorage['userUpdateble'] = true</script>
	`)
	tmpl.Execute(w, str)

}

// UserStatistic ...
func UserStatistic(w http.ResponseWriter, r *http.Request) {
	//http.ServeFile(w, r, "view/statistic.html")

	// fp, err := os.Open(dirPath + "/index.html")
	// if err != nil {
	// 	log.Println("Error 500", err.Error())
	// 	w.Write([]byte("500 internal server error"))
	// 	return
	// }
	// defer fp.Close()
	// _, err = io.Copy(w, fp)
	// if err != nil {
	// 	log.Println("Could not send file contents", err.Error())
	// 	w.Write([]byte("500 interna server error"))
	// 	return
	// }

	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("Client Succesffully Conected...")
	websockets.Reader(ws)
}

// UserUpdate ...
func UserUpdate(w http.ResponseWriter, r *http.Request) {

	userID := r.FormValue("userId")
	if userID == "" {
		http.Error(w, "data not received", 400)
		return
	}

	uID, err := strconv.Atoi(userID)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	err = UpdateUserTbl(uID, "testtask")
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	fmt.Fprintf(w, "Hey, it works !!")

}

// AuthMidlewareHandler ...
func AuthMidlewareHandler(next func(http.ResponseWriter, *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Header["Authorization"] != nil {

			// при использовании трима вылазит ошибка: invalid character 'È' looking for beginning of value
			//authToken := strings.TrimLeft(r.Header["Authorization"][0], "Bearer ")
			token, err := jwt.Parse(r.Header["Authorization"][0], func(jwtoken *jwt.Token) (interface{}, error) {
				if _, ok := jwtoken.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Token parse error")
				}
				return jwtSecretKey, nil
			})
			if err != nil {
				//fmt.Fprintf(w, err.Error())
				log.Printf("error info: %#v", err)
			}

			if token.Valid {
				next(w, r)
			}

		} else {
			fmt.Fprintf(w, "Not Athorized")
		}
	})
}
