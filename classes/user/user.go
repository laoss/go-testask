package classes

// User ...
type User struct {
	ID           int     `db:"id"`
	Balance      float64 `db:"balance"`
	BetCount     int     `db:"betCount"`
	BetSum       float64 `db:"betSum"`
	DepositCount int     `db:"depositCount"`
	DepositSum   float64 `db:"depositSum"`
	WinCount     int     `db:"winCount"`
	WinSum       float64 `db:"winSum"`
}
