package transaction

import (
	"time"
)

// Transaction ...
type Transaction struct {
	ID            int
	BetID         int
	Amount        float64
	BalanceBefore float64
	BalanceAfter  float64
	Transacted    time.Time
	Type          string
}
