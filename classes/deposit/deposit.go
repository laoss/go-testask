package deposit

import (
	"time"
)

// Deposit ...
type Deposit struct {
	ID            int
	UserID        int
	BalanceBefore float64
	BalanceAfter  float64
	AccrualDT     time.Time
}
