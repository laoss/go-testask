package main

import (
	"log"
	"net/http"

	"gitlab.com/laoss/go-testask/routers"
)

// var (
// 	userCache cache.UserCache = cache.NewRedisCache("localhost:6379", 0, 0)
// )

func main() {

	port := "9090"

	err := http.ListenAndServe(":"+port, routers.Router)
	if err != nil {
		log.Fatal("ListenAndServe", err)
	}

}
