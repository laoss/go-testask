package utils

import (
	jwt "github.com/dgrijalva/jwt-go"
)

var (
	jwtSecretKey = []byte("topsecret")
)

// GenerateJWT ...
func GenerateJWT(id int) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["uid"] = id

	tokenString, err := token.SignedString(jwtSecretKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}
