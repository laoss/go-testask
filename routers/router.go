package routers

import (
	"github.com/gorilla/mux"
	"gitlab.com/laoss/go-testask/controllers"
)

// Router ..,
var Router = mux.NewRouter()

func init() {
	//var subRtr = Router.PathPrefix("/").Subrouter()
	var subRtr = Router.Host("localhost").Subrouter()

	subRtr.HandleFunc("/", controllers.HomePage).Methods("GET")

	subRtr.HandleFunc("/user/create", controllers.CreateUserPage).Methods("GET")
	subRtr.HandleFunc("/user/create", controllers.CreateUserHandler).Methods("POST")

	subRtr.HandleFunc("/user/get", controllers.GetUserPage).Methods("GET")
	subRtr.HandleFunc("/user/get", controllers.GetUserHandler).Methods("POST")

	subRtr.HandleFunc("/user/deposit", controllers.MakeDepositPage).Methods("GET")
	subRtr.Handle("/user/deposit", controllers.AuthMidlewareHandler(controllers.MakeDepositHandler)).Methods("POST")

	subRtr.HandleFunc("/transaction", controllers.MakeTransactionPage).Methods("GET")
	subRtr.HandleFunc("/transaction", controllers.MakeTransactionHandler).Methods("POST")

	subRtr.HandleFunc("/user/update", controllers.UserUpdate).Methods("POST")

	//subRtr.HandleFunc("/statistic", controllers.UserStatistic)
}
